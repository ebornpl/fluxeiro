package bartlomiej.steidel.video;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.Toast;

/**
 *
 * @author eborn
 */
class CameraPreviewCallback implements SurfaceHolder.Callback {

    Camera camera;
    Context ctx;
    SurfaceHolder holder;

    CameraPreviewCallback(Context ctx) {
        this.ctx = ctx;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.holder = holder;
        if (camera != null) {
            try {
                camera.setPreviewDisplay(holder);
            } catch (Throwable t) {
                Log.e("PreviewDemo-surfaceCallback",
                        "Exception in setPreviewDisplay()", t);
                Toast.makeText(ctx, "Problem displaying camera preview", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        //do nothing
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        this.holder = null;
    }

    public void changeToNewCameraInstance(Camera cam) {
        this.camera=cam;
        if (this.holder != null) {
            try {
                cam.setPreviewDisplay(holder);
            } catch (Throwable t) {
                Log.e("PreviewDemo-surfaceCallback",
                        "Exception in setPreviewDisplay()", t);
                Toast.makeText(ctx, "Problem displaying camera preview", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
