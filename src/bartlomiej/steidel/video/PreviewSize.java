/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bartlomiej.steidel.video;

/**
 *
 * @author eborn
 */
public class PreviewSize implements Comparable<PreviewSize> {
    public PreviewSize(int width, int height) {
            this.width = width;
            this.height = height;
        }
        
        int width;
        int height;
        /**
         * Implemented method so that the list of sizes can be sorted
         * @param other
         * @return 
         */
        public int compareTo(PreviewSize other) {
            return (this.width*this.height)-(other.width*other.height);
        }

/**
         * Overriden method to determine duplicates in the list of sizes
         * @param o
         * @return 
         */
        @Override
        public boolean equals(Object o) {
            boolean result;
            if(super.equals(o)){
                result= true;
            }
            else{
                PreviewSize temp= (PreviewSize) o;
                if (this.width==temp.width && this.height==temp.height){
                result= true;}
                else {
                result= false;}
            }
            return result;
        }

        /**
         * Overriden toString implementation, expressing each size as WxH
         * @return 
         */
        @Override
        public String toString() {
            return width+"x"+height;
        }
}
