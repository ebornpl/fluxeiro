package bartlomiej.steidel.video;

/**
 * This class is used to convert images with YUV420 color scheme used by 
 * Android's camera preview to RGB8888 color scheme.
 * @author Bartłomiej Steidel
 */
public class ColorConverterUtil {
    static {
        System.loadLibrary("converter");
    }
    
    /**
     * Native method used for converting and resizing YUV420 frames into RGB8888 
     * frames. Most efficient when widthOut or heightOut are divisors of widthIn
     * and HeightIn accordingly.
     * @param in byte array holding YUV420 (YCbCr) pixel data 
     * @param widthIn width of incoming image
     * @param heightIn height of incoming image
     * @param out Integer array to which the converted image will be outputted.
     *              Must have the size of at least width*height.
     * @param widthOut width of output image
     * @param heightOut height of output image
     */
    public native void toRgb(byte[] in, int widthIn, int heightIn, int[] out, int widthOut, int heightOut);
}
