/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bartlomiej.steidel.video;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CameraProvider implements Camera.PreviewCallback {

    Camera cam;
    int widthIn;
    int heightIn;
    int widthOut;
    int heightOut;
    int supportsZoom = -1;
    int currentZoomLevel = 0;
    int maxZoom;
    Context ctx;
    SurfaceView preview;
    ColorConverterUtil lib;
    int[] out;
    byte[] b;
    boolean needsNew;
    Bitmap result;
    CameraPreviewCallback camPrvCallback;
    long poczatek;
    public static final int ZOOM_LEVEL_NONE = 0;
    public static final int ZOOM_LEVEL_MEDIUM = 1;
    public static final int ZOOM_LEVEL_MAX = 2;
    ArrayList<PreviewSize> fastOutputSizes;
    ArrayList<PreviewSize> supportedInputSizes;

    public CameraProvider(Context ctx, SurfaceView cameraSurface) {
        this.ctx = ctx;
        //this.lay = lay;
        preview = cameraSurface;
        lib = new ColorConverterUtil();
        camPrvCallback = new CameraPreviewCallback(ctx);
        preview.getHolder().addCallback(camPrvCallback);
        preview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public Bitmap getImage() {
        if (!needsNew) {
            needsNew = true;
            return result;
        }
        return null;
    }

    public boolean initialize() {
        if (cam == null) {
            cam = Camera.open();
        }

        camPrvCallback.changeToNewCameraInstance(cam);


        widthIn = cam.getParameters().getPreviewSize().width;
        heightIn = cam.getParameters().getPreviewSize().height;
        widthOut = widthIn;
        heightOut = heightIn;
        out = new int[widthOut * heightOut];

        this.setOutputSize(new PreviewSize(320, 240));
        startApiDependantPreview();
        needsNew = true;
        return true;
    }

    public void shutdown() {
        if (cam != null) {
            cam.setPreviewCallback(null);
            cam.stopPreview();
            cam.release();
            Log.d("ClientActivity", "Camera released.");
            cam = null;
        }
    }

    public void onPreviewFrame(byte[] arg0, Camera arg1) {
        //If b is not null, it means that method startApiDependantPreview used 
        //preview from API version 8 - we are using this again.
        if (b != null) {
            arg0 = b;
            try {
                Method addCallbackBuffer = Camera.class.getMethod("addCallbackBuffer", byte[].class);
                addCallbackBuffer.invoke(cam, b);
            } catch (Exception ex) {
                Log.d("fluxeiro", "problem with method from API 8", ex);
                cam.setPreviewCallback(this);
            }
        }
        if (needsNew) {
            lib.toRgb(arg0, widthIn, heightIn, out, widthOut, heightOut);
            result = Bitmap.createBitmap(out, widthOut, heightOut, Bitmap.Config.ARGB_8888);
            //Log.d("--------------Converter", "converted in: "+(System.currentTimeMillis()-poczatek));
            needsNew = false;
        }
    }

    /**
     * Tries to change the output video's size. This will work for all sizes
     * lower or equal to the native size of camera preview. However, in order to
     * get best framerate, use one of the sizes specified by
     * {@link getFastOutputSizes()}
     *
     * @param width
     * @param height
     * @return true if succeeded, or false if any of the specified dimensions
     * was bigger than default camera preview size.
     */
    public void setOutputSize(PreviewSize size) {
        if (supportedInputSizes == null) {
            getFastOutputSizes();
        }
        PreviewSize max = supportedInputSizes.get(supportedInputSizes.size() - 1);
        //only if proper arguments were given we should concentrate on changing
        //the size.
        if (size.width <= max.width && size.height <= max.height && size.width > 0 && size.height > 0) {
            //if size comes from the list of fast output sizes, we should
            //adopt the preview size to it
            if (fastOutputSizes.indexOf(size) >= 0) {

                int optimalWidth = widthIn;
                int optimalHeight = heightIn;
                //looking for the "correct" size for the given input size:
                //e.g. for 120x120, the correct input size would be 120x120,
                //240x240 or 360x360 - other sizes will not be so easily
                //converted so we will lose the performance
                for (PreviewSize s : supportedInputSizes) {
                    if (size.equals(s) || new PreviewSize(size.width * 2, size.height * 2).equals(s) || new PreviewSize(size.width * 3, size.height * 3).equals(s)) {
                        optimalWidth = s.width;
                        optimalHeight = s.height;
                        break;
                    }
                }
                //if the chosen optimal size is different from current settings
                if (optimalWidth != widthIn || optimalHeight != heightIn) {
                    //stopping the preview for the moment
                    cam.setPreviewCallback(null);
                    cam.stopPreview();
                    //setting new size
                    Camera.Parameters params = cam.getParameters();
                    params.setPreviewSize(optimalWidth, optimalHeight);
                    cam.setParameters(params);
                    //updating all the values locally for converter class
                    widthIn = optimalWidth;
                    heightIn = optimalHeight;
                    widthOut = size.width;
                    heightOut = size.height;
                    //restarting camera preview
                    startApiDependantPreview();
                }
            }
            //setting the output values to the ones given by parameter
            widthOut = size.width;
            heightOut = size.height;
            out = new int[widthOut * heightOut];
        }
    }

    /**
     * Returns a list of suggested preview sizes. It will both contain the sizes
     * natively supported by hardware and sizes which take advantage of faster
     * resizing algorithm. They will be sorted in ascending manner by size
     * (width*height).
     *
     * @return A list of preview sizes for which the output framerate should be
     * best
     */
    public ArrayList<PreviewSize> getFastOutputSizes() {
        //it should generete this list only on first call, since it will not change
        if (fastOutputSizes == null) {
            supportedInputSizes = new ArrayList<PreviewSize>();
            fastOutputSizes = new ArrayList<PreviewSize>();
            //Obtaining the list of sizes supported by the phone's hardware
            ArrayList<Camera.Size> supportedSizes = new ArrayList<Camera.Size>();
            supportedSizes.add(cam.getParameters().getPreviewSize());
            try {
                Method getPreviewSizes = Camera.Parameters.class.getMethod(
                        "getSupportedPreviewSizes", null);
                supportedSizes = new ArrayList<Camera.Size>((List<Camera.Size>) getPreviewSizes.invoke(cam.getParameters(), (Object[]) null));
                //supportedSizes = new ArrayList<Camera.Size>(cam.getParameters().getSupportedPreviewSizes());
            } catch (Exception nsme) {
                Log.d("Fluxeiro", "Problem with method from API lvl 8", nsme);
            }
            //going through all of the supported sizes
            for (Camera.Size size : supportedSizes) {
                //creating temporary Size object
                PreviewSize temp = new PreviewSize(size.width, size.height);
                //adding current size to the list of supported input sizes
                supportedInputSizes.add(temp);
                //the 3 following if statements do the following:
                //1. They add the current size if it is not yet in the
                //   list of fast output sizes
                //2. They try to add width/2,height/2 and width/3,height/3 
                //   sizes, only if the dimensions are divisible by 2 and/or 3
                //   It is necessary because for such values the conversion
                //   algorithm will be faster.
                if (fastOutputSizes.indexOf(temp) < 0) {
                    fastOutputSizes.add(temp);
                }
                if (temp.width % 2 == 0 && temp.height % 2 == 0 && fastOutputSizes.indexOf(new PreviewSize(temp.width / 2, temp.height / 2)) < 0) {
                    fastOutputSizes.add(new PreviewSize(temp.width / 2, temp.height / 2));
                }
                if (temp.width % 3 == 0 && temp.height % 3 == 0 && fastOutputSizes.indexOf(new PreviewSize(temp.width / 3, temp.height / 3)) < 0) {
                    fastOutputSizes.add(new PreviewSize(temp.width / 3, temp.height / 3));
                }
            }
            //We are sorting the list by the amount of pixels in total (see
            //method "compareTo" from IPhoneCameraBinder.Size)
            Collections.sort(supportedInputSizes);
            Collections.sort(fastOutputSizes);
        }
        return fastOutputSizes;
    }

    public String getCurrentResolution() {
        return widthOut + "x" + heightOut;
    }

    /**
     * Returns current zoom level in a 3-level scale: 0-none 1-medium 2-max, or
     * -1 if camera was not initialized
     *
     * @return zoom level: 0-none 1-medium 2-max, or -1 if camera was not
     * initialized
     */
    public int getCurrentZoom() {
        if (cam == null) {
            return -1;
        }
        return currentZoomLevel;
    }

    public boolean isZoomSupported() {
        // -1 is an initial value, which means it is not yet initialized
        if (supportsZoom == -1) {
            if ("true".equals(cam.getParameters().get("zoom-supported")) && (maxZoom = cam.getParameters().getInt("max-zoom")) != 0) {
                supportsZoom = 1;
            } else {
                supportsZoom = 0;
            }
        }
        if (supportsZoom == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Sets the zoom level of camera. Call {@link isZoomSupported()} first to
     * check if zooming is supported - if not, this method won't do anything
     *
     * @param zoom zoom level: 0-none 1-medium 2-max (see constants
     * ZOOM_LEVEL_*)
     */
    public void setZoom(int zoom) {
        //do this only if already initialized.
        if (cam != null && isZoomSupported()) {
            cam.setPreviewCallback(null);
            cam.stopPreview();
            Camera.Parameters params = cam.getParameters();

            if (zoom == ZOOM_LEVEL_MEDIUM) {
                params.set("zoom", maxZoom / 2);
                currentZoomLevel = ZOOM_LEVEL_MEDIUM;
            } else if (zoom == ZOOM_LEVEL_MAX) {
                params.set("zoom", maxZoom);
                currentZoomLevel = ZOOM_LEVEL_MAX;
            } else if (zoom == ZOOM_LEVEL_NONE) {
                params.set("zoom", 0);
                currentZoomLevel = ZOOM_LEVEL_NONE;
            }
            cam.setParameters(params);
            startApiDependantPreview();
        }
        //if wrong value was passed - do nothing
    }

    public void autoFocus() {
        cam.autoFocus(null);
    }

    public void setScreenPreviewEnabled(boolean state) {
        if (cam != null) {
            cam.setPreviewCallback(null);
            cam.stopPreview();
            if (state) {
                preview.setEnabled(true);
                preview.setVisibility(SurfaceView.VISIBLE);
                camPrvCallback.changeToNewCameraInstance(cam);
            } else {
                try {
                    cam.setPreviewDisplay(null);
                } catch (IOException ex) {
                    Log.d("fluxeiro", "", ex);
                }
                preview.setEnabled(false);
                preview.setVisibility(SurfaceView.GONE);
            }
            startApiDependantPreview();
        }
    }
    
    public boolean getScreenPreviewEnabled(){
        return preview.isEnabled();
    }

    private void startApiDependantPreview() {
        try {
            //All these methods are from API level 8
            Class ImageFormat=Class.forName("android.graphics.ImageFormat");
            Method setPreviewCallbackWithBuffer = Camera.class.getMethod("setPreviewCallbackWithBuffer", Camera.PreviewCallback.class);
            setPreviewCallbackWithBuffer.invoke(cam, this);
            Method getBitsPerPixel = ImageFormat.getMethod("getBitsPerPixel", int.class);
            int bitsPerPixel=(Integer) getBitsPerPixel.invoke(null, cam.getParameters().getPreviewFormat());
            b = new byte[widthIn * heightIn * bitsPerPixel / 8];
            Method addCallbackBuffer = Camera.class.getMethod("addCallbackBuffer", byte[].class);
            addCallbackBuffer.invoke(cam, b);
        } catch (Exception nsme) {
            Log.d("Fluxeiro", "Problem with method from API lvl 8", nsme);
            cam.setPreviewCallback(this);
        }
        cam.startPreview();
    }
}
