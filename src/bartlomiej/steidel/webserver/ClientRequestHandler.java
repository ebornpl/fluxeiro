/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bartlomiej.steidel.webserver;

import android.content.Intent;
import android.graphics.Bitmap;
//import android.support.v4.content.LocalBroadcastManager;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Locale;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.entity.ContentProducer;
import org.apache.http.entity.EntityTemplate;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import bartlomiej.steidel.video.CameraProvider;
import bartlomiej.steidel.video.PreviewSize;

class ClientRequestHandler implements HttpRequestHandler {

    private CameraProvider camProvider;
    public static final String ACTION_CLIENT_CONNECTED = "bartlomiej.steidel.ACTION_CLIENT_CONNECTED";
//    private LocalBroadcastManager mLocalBroadcastManager;

    public ClientRequestHandler(CameraProvider camProvider) {
        this.camProvider = camProvider;
//        mLocalBroadcastManager = LocalBroadcastManager.getInstance(null);
    }

    public void handle(
            final HttpRequest request,
            final HttpResponse response,
            final HttpContext context) throws HttpException, IOException {

        String method = request.getRequestLine().getMethod().toUpperCase(Locale.ENGLISH);
        if (!method.equals("GET") && !method.equals("HEAD") && !method.equals("POST")) {
            throw new MethodNotSupportedException(method + " method not supported");
        }
        String target = request.getRequestLine().getUri();

        if (target.equals("/mjpeg")) {
            MJPEGresponse(response);
        } else if (target.startsWith("/js")) {
            jsResponse(response);
        } else if (target.startsWith("/img")) {
            imgResponse(response);
        } else if (target.startsWith("/cfg")) {
            //check if any parameters were sent
            if (target.length() > 5) {
                //cut just these params
                target = target.substring(target.indexOf('?') + 1);
                cameraConfigResponse(response, target);
            } else {
                //if not, just pass null
                cameraConfigResponse(response, null);
            }
        } else {
            mainMenuResponse(response);
        }

        System.out.println("Served");
    }

    private void MJPEGresponse(HttpResponse response) {
//        Intent clientConnected = new Intent(ACTION_CLIENT_CONNECTED).putExtra("ip", "");
//        mLocalBroadcastManager.sendBroadcast(clientConnected);
        response.setStatusCode(HttpStatus.SC_OK);
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");

        EntityTemplate body = new EntityTemplate(new ContentProducer() {

            public void writeTo(final OutputStream outstream) throws IOException {
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outstream));
                BufferedOutputStream bos = new BufferedOutputStream(outstream, 16192);
                out.write("--jakasgranica\n");
                out.flush();
                Bitmap bmp;
                while (true) {
                    bmp = camProvider.getImage();
                    if (bmp != null) {
                        //long poczatek = System.currentTimeMillis();
                        out.write("Content-type: image/jpeg\n\n");
                        out.flush();
                        bmp.compress(Bitmap.CompressFormat.JPEG, 30, bos);
                        bos.flush();
                        out.write("--jakasgranica\n");
                        out.flush();
                        bmp.recycle();
                        //Log.d("--------Video", "sent in: " + (System.currentTimeMillis() - poczatek));
                    }
                }
            }
        });
        body.setContentType("multipart/x-mixed-replace; boundary=jakasgranica");
        response.setEntity(body);
    }

    private void mainMenuResponse(HttpResponse response) {
        response.setStatusCode(HttpStatus.SC_OK);
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");

        EntityTemplate body = new EntityTemplate(new ContentProducer() {

            public void writeTo(final OutputStream outstream) throws IOException {
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outstream));
                out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"
                        + "\n<HTML>"
                        + "\n<HEAD>"
                        + "\n<TITLE>Fluxeiro</TITLE>"
                        + "\n</HEAD>"
                        + "\n<BODY><A HREF=\"mjpeg\" title=\"MJPEG stream\">MJPEG stream</a><br />"
                        + "<A HREF=\"js\" title=\"Javascript stream\">Javascript stream</a><br />"
                        + "<A HREF=\"img\" title=\"Single snapshot\">Single snapshot</a><br />"
                        + "<A HREF=\"cfg\" title=\"Settings\">Settings</a><br />"
                        + "</BODY>"
                        + "\n</HTML>");
                out.flush();
            }
        });
        body.setContentType("text/html; charset=UTF-8");
        response.setEntity(body);
    }

    private void cameraConfigResponse(HttpResponse response, String params) {
        response.setStatusCode(HttpStatus.SC_OK);
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        //if parameters were null, just print out normally
        if (params != null) {
            //if not, some action is required
            if (params.startsWith("res")) {
                //this means resolution change is requested
                int width = 0;
                int height = 0;
                params = params.substring(params.indexOf('=') + 1);
                try {
                    width = Integer.parseInt(params.substring(0, params.indexOf('x')));
                    height = Integer.parseInt(params.substring(params.indexOf('x') + 1));
                } catch (NumberFormatException ex) {
                    width = 0;
                    height = 0;
                }
                if (width > 0 && height > 0) {
                    camProvider.setOutputSize(new PreviewSize(width, height));
                }
            } else if (params.startsWith("zoom")) {
                //this means zoom change was requested
                params = params.substring(params.indexOf('=') + 1);
                if (params.startsWith("none")) {
                    camProvider.setZoom(CameraProvider.ZOOM_LEVEL_NONE);
                } else if (params.startsWith("medium")) {
                    camProvider.setZoom(CameraProvider.ZOOM_LEVEL_MEDIUM);
                } else if (params.startsWith("max")) {
                    camProvider.setZoom(CameraProvider.ZOOM_LEVEL_MAX);
                }
            } else if (params.startsWith("af")) {
                //this means autofocus was requested
                camProvider.autoFocus();
            }
        }

        EntityTemplate body = new EntityTemplate(new ContentProducer() {

            public void writeTo(final OutputStream outstream) throws IOException {
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outstream));
                out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"
                        + "\n<HTML>"
                        + "\n<HEAD>"
                        + "\n<TITLE>Fluxeiro</TITLE>"
                        + "\n</HEAD>"
                        + "\n<BODY>Current resolution: " + camProvider.getCurrentResolution() + "<br />"
                        + "<form name=\"resform\" action=\"cfg\" method=\"get\">"
                        + "<select name='res'>");

                for (PreviewSize size : camProvider.getFastOutputSizes()) {
                    out.write("<option value='" + size.toString() + "'>" + size.toString() + "</option>");
                }

                out.write("</select>"
                        + "<input type='submit' value='Apply' />"
                        + "</form>"
                        + "Current zoom: None<br /><form name='zoomform' action='cfg' method='get'>"
                        + "<input type='radio' name='zoom' checked='true' value='none' /> None"
                        + "<input type='radio' name='zoom' value='medium' /> Medium "
                        + "<input type='radio' name='zoom' value='max' /> Max "
                        + "<input type='submit' value='Apply' />"
                        + "</form>"
                        + "<A HREF='.' title='Back to main'>&lt;- Back to main</a><br />"
                        + "</BODY>"
                        + "\n</HTML>");
                out.flush();
            }
        });
        body.setContentType("text/html; charset=UTF-8");
        response.setEntity(body);
    }

    private void jsResponse(HttpResponse response) {
        response.setStatusCode(HttpStatus.SC_OK);
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        EntityTemplate body = new EntityTemplate(new ContentProducer() {

            public void writeTo(final OutputStream outstream) throws IOException {
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(outstream));
                out.write("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"
                        + "\n<HTML>"
                        + "\n<HEAD>"
                        + "\n<TITLE>Fluxeiro</TITLE>"
                        + "<script language='javascript' type='text/javascript'>"
                        + "function updateZoom() {"
                        + "var selection = document.getElementsByName('zoom');"
                        + "for (i=0; i<selection.length; i++)"
                        + "if (selection[i].checked == true){"
                        + "img = new Image(); img.src = 'cfg?zoom='+selection[i].value;}}"
                        + "function updateRes(){"
                        + "img = new Image(); img.src = 'cfg?res='+document.getElementsByName('res')[0].options[document.getElementsByName('res')[0].selectedIndex].value;}"
                        + "</script>"
                        + "\n</HEAD>"
                        + "\n<BODY>"
                        + "<img src='img' onLoad=\"document.getElementById('imgzor').src = 'img?' + new Date().getTime()\" id='imgzor'><br />"
                        + "<button onclick=\"img = new Image(); img.src = 'cfg?af';\">Autofocus</button>"
                        + "Resolution: <form name=\"resform\" action=\"cfg\" method=\"get\">"
                        + "<select name='res'>");

                for (PreviewSize size : camProvider.getFastOutputSizes()) {
                    out.write("<option value='" + size.toString() + "'>" + size.toString() + "</option>");
                }

                out.write("</select>"
                        + "<input type='button' value='Apply' onclick='updateRes()'/>"
                        + "</form>"
                        + "Zoom: <form name='zoomform' action='cfg' method='get'>"
                        + "<input type='radio' name='zoom' checked='true' value='none' /> None"
                        + "<input type='radio' name='zoom' value='medium' /> Medium "
                        + "<input type='radio' name='zoom' value='max' /> Max "
                        + "<input type='button' value='Apply' onclick='updateZoom();'/>"
                        + "</form>"
                        + "<A HREF='.' title='Back to main'>&lt;- Back to main</a><br />"
                        + "</BODY>"
                        + "\n</HTML>");
                out.flush();
            }
        });
        body.setContentType("text/html; charset=UTF-8");
        response.setEntity(body);
    }

    private void imgResponse(HttpResponse response) {
        response.setStatusCode(HttpStatus.SC_OK);
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");

        EntityTemplate body = new EntityTemplate(new ContentProducer() {

            public void writeTo(final OutputStream outstream) throws IOException {
                BufferedOutputStream bos = new BufferedOutputStream(outstream, 16192);
                Bitmap bmp;
                while ((bmp = camProvider.getImage()) == null) {
                }

                bmp.compress(Bitmap.CompressFormat.JPEG, 30, bos);
                bos.flush();
                bmp.recycle();
            }
        });
        body.setContentType("image/jpeg");
        response.setEntity(body);
    }
}
