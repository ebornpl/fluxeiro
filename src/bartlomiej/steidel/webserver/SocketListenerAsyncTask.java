/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bartlomiej.steidel.webserver;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import bartlomiej.steidel.video.CameraProvider;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.*;

public class SocketListenerAsyncTask extends AsyncTask<Void, Integer, Boolean> {

    private final ServerSocket serversocket;
    private final HttpParams params;
    private final HttpService httpService;
    private ArrayList<Socket> connections;
    private CameraProvider camProvider;
    Context ctx;
    ProgressDialog pd;
    private final int CAMERA_STARTED = 777;
    private LocalBroadcastManager mLocalBroadcastManager;
    public static final String ACTION_SERVER_STARTED = "bartlomiej.steidel.ACTION_SERVER_STARTED";

    public SocketListenerAsyncTask(int port, CameraProvider camProvider, Context ctx) throws IOException {
        this.ctx = ctx;
        this.connections = new ArrayList<Socket>();
        this.camProvider = camProvider;
        this.serversocket = new ServerSocket(port);
        // Timeout for checking regularly if the thread was interrupted.
        this.serversocket.setSoTimeout(1000);
        this.params = new BasicHttpParams();
        this.params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 5000).setIntParameter(CoreConnectionPNames.SOCKET_BUFFER_SIZE, 8 * 1024).setBooleanParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, false).setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true).setParameter(CoreProtocolPNames.ORIGIN_SERVER, "HttpComponents/1.1");
        this.mLocalBroadcastManager = LocalBroadcastManager.getInstance(ctx);
        // Set up the HTTP protocol processor
        BasicHttpProcessor httpproc = new BasicHttpProcessor();
        httpproc.addInterceptor(new ResponseDate());
        httpproc.addInterceptor(new ResponseServer());
        httpproc.addInterceptor(new ResponseContent());
        httpproc.addInterceptor(new ResponseConnControl());

        // Set up request handlers
        HttpRequestHandlerRegistry reqistry = new HttpRequestHandlerRegistry();
        reqistry.register("*", new ClientRequestHandler(camProvider));

        // Set up the HTTP service
        this.httpService = new HttpService(
                httpproc,
                new DefaultConnectionReuseStrategy(),
                new DefaultHttpResponseFactory());
        this.httpService.setParams(this.params);
        this.httpService.setHandlerResolver(reqistry);
    }

    private void releaseAllResources() {
        try {
            this.serversocket.close();
            for (Socket socket : connections) {
                socket.close();
            }
            camProvider.shutdown();
        } catch (IOException ex) {
            Log.d("ClientActivity", "Problem closing socket", ex);
        }
    }

    @Override
    protected Boolean doInBackground(Void... arg0) {
        System.out.println("Listening on port " + this.serversocket.getLocalPort());
        Intent serverStarted = new Intent(ACTION_SERVER_STARTED).putExtra("port", this.serversocket.getLocalPort());
        mLocalBroadcastManager.sendBroadcast(serverStarted);
        this.camProvider.initialize();
        Integer[] cameraStarted = {CAMERA_STARTED};
        this.publishProgress(cameraStarted);
        while (!isCancelled()) {
            try {
                // Set up HTTP connection
                Socket socket = this.serversocket.accept();
                DefaultHttpServerConnection conn = new DefaultHttpServerConnection();
                System.out.println("Incoming connection from " + socket.getInetAddress());
                conn.bind(socket, this.params);
                connections.add(socket);
                // Start worker thread
                Thread t = new ClientConnectionThread(this.httpService, conn);
                t.start();
            } catch (InterruptedIOException ex) {
                //this will be hit whenever accept() times out. We just want
                //it to proceed with the loop if thread is not interrupted.
            } catch (IOException e) {
                Log.d("Video", "Got IO Exception while accepting conenction", e);
                break;
            }
        }
        return true;
    }

    @Override
    protected void onPreExecute() {
        pd = ProgressDialog.show(ctx, "Please wait", "Starting camera...", true, true);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (values[0] == CAMERA_STARTED) {
            pd.dismiss();
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        releaseAllResources();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        releaseAllResources();
    }
}
