package bartlomiej.steidel.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import bartlomiej.steidel.video.CameraProvider;
import bartlomiej.steidel.webserver.SocketListenerAsyncTask;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class MainActivity extends Activity {

    private EditText serverIp;
    private Button startStreamBtn;
    private TextView infoBar;
    private SurfaceView cameraSurface;
    private int port = 8080;
    SocketListenerAsyncTask httplistener;
    CameraProvider camProvider;
    BroadcastReceiver serverStatusReceiver;
    LocalBroadcastManager mBroadcastManager;
    BroadcastReceiver wifiConnectionReceiver;
    Toast notificationToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        serverIp = (EditText) findViewById(R.id.editPort);
        startStreamBtn = (Button) findViewById(R.id.start);
        startStreamBtn.getBackground().setAlpha(200);
        infoBar = (TextView) findViewById(R.id.infoBar);
        notificationToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        cameraSurface = (SurfaceView) findViewById(R.id.cameraSurface);

        camProvider = new CameraProvider(MainActivity.this, cameraSurface);

        wifiConnectionReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent intent) {
                if (intent.getAction().equals(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION)) {
                    if (!intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false)) {
                        notificationToast.setText("You have lost internet connection!");
                        notificationToast.show();
                        stopServer();
                    }
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        registerReceiver(wifiConnectionReceiver, intentFilter);

        mBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter filter = new IntentFilter();
//        filter.addAction(RequestListenerThread.ACTION_CLIENT_CONNECTED);
        filter.addAction(SocketListenerAsyncTask.ACTION_SERVER_STARTED);
        serverStatusReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
//                if (intent.getAction().equals(RequestListenerThread.ACTION_CLIENT_CONNECTED)) {
//                    if(notificationToast==null)
//                    notificationToast=Toast.makeText(MainActivity.this, "New client "+intent.getStringExtra("ip"), Toast.LENGTH_SHORT);
//                    else {
//                        notificationToast.setText("New client "+intent.getStringExtra("ip"));
//                    }
//                    notificationToast.show();
//                } else if (intent.getAction().equals(RequestListenerThread.ACTION_SERVER_STARTED)){
                infoBar.setBackgroundColor(0x7700FF00);
                infoBar.setText("Waiting for clients at " + getLocalIpAddress() + ":" + intent.getIntExtra("port", -1));
//                }
            }
        };
        mBroadcastManager.registerReceiver(serverStatusReceiver, filter);
    }

    public void toggleButtonClicked(View v) {
        if (httplistener == null || httplistener.getStatus() != AsyncTask.Status.RUNNING || httplistener.isCancelled()) {
            if (getLocalIpAddress() == null) {
                notificationToast.setText("Please check your internet connection");
                notificationToast.show();
                return;
            }
            try {
                httplistener = new SocketListenerAsyncTask(port, camProvider, this);
                httplistener.execute();
                startStreamBtn.setBackgroundResource(R.drawable.button_off);
                startStreamBtn.getBackground().setAlpha(200);
            } catch (IOException ex) {
                Log.d("Video", "problem starting server on this port");
                notificationToast.setText("Chosen port (" + port + ") is still in use. Wait a moment or choose another.");
                notificationToast.show();
            } catch (NumberFormatException ex) {
                Log.d("Video", "wrong port format");
            }
        } else {
            stopServer();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(wifiConnectionReceiver);
        stopServer();
    }

    private void stopServer() {
        if (httplistener != null && !httplistener.isCancelled() && httplistener.getStatus() == AsyncTask.Status.RUNNING) {
            httplistener.cancel(true);
            startStreamBtn.setBackgroundResource(R.drawable.button_on);
            startStreamBtn.getBackground().setAlpha(200);
            infoBar.setBackgroundColor(0x77FF0000);
            infoBar.setText("Server NOT running.");
        }
        camProvider.shutdown();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuPort:
                    final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    final EditText input = new EditText(this);
                    alert.setTitle("Type the new port number");
                    input.setText(Integer.toString(port));
                    input.setInputType(InputType.TYPE_CLASS_NUMBER);
                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(5);
                    input.setFilters(FilterArray);
                    alert.setView(input);
                    alert.setPositiveButton("Apply", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            String value = input.getText().toString().trim();
                            try {
                                port = Integer.parseInt(value);
                            } catch (NumberFormatException ex) {
                                notificationToast.setText("Wrong port format");
                                notificationToast.show();
                            }

                        }
                    });
                    alert.setNeutralButton("Use next (" + (port + 1) + ")", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int arg1) {
                            port=port+1;
                            dialog.cancel();
                        }
                    });
                    alert.setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    dialog.cancel();
                                }
                            });
                
                alert.show();
                break;
            case R.id.menuPreview:
                camProvider.setScreenPreviewEnabled(!camProvider.getScreenPreviewEnabled());
                break;
        }
        return true;
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("getIP", ex.toString());
        }
        return null;
    }
}
