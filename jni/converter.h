#include <jni.h>

JNIEXPORT void Java_bartlomiej_steidel_video_ColorConverterUtil_toRgb
(JNIEnv* env, jobject object, jbyteArray pinArray, jint widthIn, jint heightIn, jintArray poutArray, jint widthOut, jint heightOut);

void color_convert_common(
    unsigned char *pY, unsigned char *pUV,
    int widthIn, int heightIn,
    int *buffer, int widthOut, int heightOut);