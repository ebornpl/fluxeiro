#include "converter.h"
#define NULL 0
#ifndef max
#define max(a,b) ({typeof(a) _a = (a); typeof(b) _b = (b); _a > _b ? _a : _b; })
#define min(a,b) ({typeof(a) _a = (a); typeof(b) _b = (b); _a < _b ? _a : _b; })
#endif

/*
 * convert a yuv420 array to a rgb array
 */
JNIEXPORT void JNICALL Java_bartlomiej_steidel_video_ColorConverterUtil_toRgb
(JNIEnv* env, jobject object, jbyteArray pinArray, jint widthIn, jint heightIn, jintArray poutArray, jint widthOut, jint heightOut) {
    jbyte *inArray;
    jint *outArray;
    inArray = (*env)->GetByteArrayElements(env, pinArray, JNI_FALSE);
    outArray = (*env)->GetIntArrayElements(env, poutArray, JNI_FALSE);
    //see http://java.sun.com/docs/books/jni/html/functions.html#100868
    //If isCopy is not NULL, then *isCopy is set to JNI_TRUE if a copy is made; if no copy is made, it is set to JNI_FALSE.	

    color_convert_common(inArray, inArray + widthIn * heightIn,
            widthIn, heightIn,
            outArray, widthOut, heightOut);


    //release arrays:
    (*env)->ReleaseByteArrayElements(env, pinArray, inArray, 0);
    (*env)->ReleaseIntArrayElements(env, poutArray, outArray, 0);
}

/*
   YUV 4:2:0 image with a plane of 8 bit Y samples followed by an interleaved
   U/V plane containing 8 bit 2x2 subsampled chroma samples.
   except the interleave order of U and V is reversed.

                        H V
   Y Sample Period      1 1
   U (Cb) Sample Period 2 2
   V (Cr) Sample Period 2 2
 */


/*
 size of a char:
 find . -name limits.h -exec grep CHAR_BIT {} \;
 */

const int bytes_per_pixel = 2;

void color_convert_common(
        unsigned char *pY, unsigned char *pUV,
        int widthIn, int heightIn,
        int *buffer, int widthOut, int heightOut) {
    int i, j;
    int translatedI;
    int translatedJ;
    int nR, nG, nB;
    int nY, nU, nV;
    unsigned char* tempVar;
    int tempVar2;
    unsigned char *out = (unsigned char*) buffer;
    int offset = 0;

    //if any of the Out dimensions is not a divider of In dimensions - 
    //we have to use slower resizing algorithm with floats
    if (widthIn%widthOut || heightIn%heightOut) {        
        
        float heightRatio = heightIn / (float)heightOut;
        float widthRatio = widthIn / (float)widthOut;
        // YUV 4:2:0
        for (i = 0; i < heightOut; i++) {
            translatedI = (int) i*heightRatio;
            for (j = 0; j < widthOut; j++) {
                translatedJ = (int) j*widthRatio;
                nY = *(pY + translatedI * widthIn + translatedJ);
                nV = *(tempVar=(pUV + (translatedI >> 1) * widthIn + ((translatedJ>>1)<<1)));
                nU = *(++tempVar);

                // Yuv Convert
                nY -= 16;
                nU -= 128;
                nV -= 128;

                if (nY < 0)
                    nY = 0;

                nB = (int) ((tempVar2=1192 * nY) + 2066 * nU);
                nG = (int) (tempVar2 - 833 * nV - 400 * nU);
                nR = (int) (tempVar2 + 1634 * nV);

                nR = min(262143, max(0, nR));
                nG = min(262143, max(0, nG));
                nB = min(262143, max(0, nB));

                nR >>= 10;
                nR &= 0xff;
                nG >>= 10;
                nG &= 0xff;
                nB >>= 10;
                nB &= 0xff;

                out[offset++] = (unsigned char) nB;
                out[offset++] = (unsigned char) nG;
                out[offset++] = (unsigned char) nR;
                out[offset++] = 255;
            }
        }
    }
    else { 
        
        int heightStep = heightIn / heightOut;
        int widthStep = widthIn / widthOut;
        // YUV 4:2:0
        for (i = 0; i < heightIn; i+=heightStep) {
            for (j = 0; j < widthIn; j+=widthStep) {
                nY = *(pY + i * widthIn + j);
                nV = *(tempVar=(pUV + (i >> 1) * widthIn + ((j>>1)<<1)));
                nU = *(++tempVar);

                // Yuv Convert
                nY -= 16;
                nU -= 128;
                nV -= 128;

                if (nY < 0)
                    nY = 0;

                nB = (int) ((tempVar2=1192 * nY) + 2066 * nU);
                nG = (int) (tempVar2 - 833 * nV - 400 * nU);
                nR = (int) (tempVar2 + 1634 * nV);

                nR = min(262143, max(0, nR));
                nG = min(262143, max(0, nG));
                nB = min(262143, max(0, nB));

                nR >>= 10;
                nR &= 0xff;
                nG >>= 10;
                nG &= 0xff;
                nB >>= 10;
                nB &= 0xff;

                out[offset++] = (unsigned char) nB;
                out[offset++] = (unsigned char) nG;
                out[offset++] = (unsigned char) nR;
                out[offset++] = 255;
            }
        }
    }
}
